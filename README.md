# php-xattr

This library is intended for accessing and manipulating a file's extended attributes.

This probably only works on Linux, and everything has to be set up just right for this to work.

Ideally, you should have the [xattr extension](https://www.php.net/manual/en/intro.xattr.php) installed.
If you don't have it installed, we'll try to use the [`attr`](https://savannah.nongnu.org/projects/attr)
command.

This library is designed to fail silently. This means that if neither the xattr extension, or the attr command
are available, it will simply do nothing. This will also happen if the filesystem doesn't support extended
attributes.

## Installation

```shell
composer require danjones000/php-xattr
```

## Usage

```php
use Danjones\Xattr\Xattr;
use Danjones\Xattr\File;

$file = 'path/to/file';
$xattr = new Xattr();
$attributes = $xattr->list($file); // Returns array of attribute names, without values
$value = $xattr->get($file, $name); // Returns a string, or null if not available
$xattr->set($file, $name, 'new-value'); // This will return nothing.

$file2 = 'path/to/another/file';
$xattr->copy($file, $name, $file2); // Copies the attribute named $name from $file to $file2
$xattr->clone($file, $file2); // Copies all attributes from $file to $file2. Existing attributes in $file2 will remain intact

// Use File object when doing multiple operations on a single file
// All methods from $xattr are available. void methods from Xattr return the object, so they can be chained.
$fileObj = $xattr->file($file); // or new File($file);
$file2Obj = $xattr->file($file2);
$fileObj->set($name, 'another-value')->copy($name, $file2Obj);
$fileObj->clone($file2); // Can use either path, or another File object
$file2Obj->list(); // Will have all the attributes added by $fileObj
```
