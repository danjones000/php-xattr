<?php

declare(strict_types=1);

namespace Danjones\Xattr;

/**
 * Gives access to a file's extended attributes.
 *
 * Will use the xattr extension if available. If not available,
 * it will attempt to use the attr shell command.
 *
 * This is designed to fail silently. If it doesn't work, it just
 * does nothing.
 */
class Xattr
{
    /**
     * Copies all attributes from $source to $dest
     *
     * @param string $source
     * @param string $dest
     */
    public function clone(string $source, string $dest): void
    {
        $attributes = $this->list($source);
        foreach ($attributes as $key) {
            $this->copy($source, $key, $dest);
        }
    }

    /**
     * Copies $key attributes from $source to $dest
     *
     * @param string $source
     * @param string $key
     * @param string $dest
     */
    public function copy(string $source, string $key, string $dest): void
    {
        $value = $this->get($source, $key);
        if ($value) {
            $this->set($dest, $key, $value);
        }
    }

    /**
     * Returns the $key attributes from $file
     *
     * @param string $file
     * @param string $key
     * @return string|null
     */
    public function get(string $file, string $key): ?string
    {
        if (function_exists('xattr_get')) {
            return xattr_get($file, $key) ?: null;
        }

        if (!$this->hasAttr()) {
            return null;
        }

        $out = [];
        exec(sprintf('attr -qg %s %s 2>/dev/null', escapeshellarg($key), escapeshellarg($file)), $out);
        $out = trim(implode("\n", $out));

        return $out ?: null;
    }

    /**
     * Sets the $key attributes to $value on $file.
     *
     * If $value is null, or empty, remove the attribute.
     *
     * @param string $file
     * @param string $key
     * @param string $value
     */
    public function set(string $file, string $key, ?string $value): void
    {
        if (empty($value)) {
            $this->remove($file, $key);

            return;
        }

        if (function_exists('xattr_set')) {
            xattr_set($file, $key, $value);

            return;
        }

        if (!$this->hasAttr()) {
            return;
        }

        exec(sprintf(
            'attr -qs %s -V %s %s 2>/dev/null',
            escapeshellarg($key),
            escapeshellarg($value),
            escapeshellarg($file)
        ));
    }

    /**
     * Removes the $key attribute from $file
     *
     * @param string $file
     * @param string $key
     */
    public function remove(string $file, string $key): void
    {
        if (function_exists('xattr_remove')) {
            xattr_remove($file, $key);

            return;
        }

        if (!$this->hasAttr()) {
            return;
        }

        exec(sprintf('attr -qr %s %s 2>/dev/null', escapeshellarg($key), escapeshellarg($file)));
    }

    /**
     * Returns a list of all attribute names on $file
     *
     * @param string $file
     * @return array
     */
    public function list(string $file): array
    {
        if (function_exists('xattr_list')) {
            return xattr_list($file);
        }

        if (!$this->hasAttr()) {
            return [];
        }

        $all = [];
        exec(sprintf('attr -ql %s 2>/dev/null', escapeshellarg($file)), $all);
        $security = [];
        exec(sprintf('attr -Sql %s 2>/dev/null', escapeshellarg($file)), $security);

        return array_values(array_diff($all, $security));
    }

    /**
     * Creates a File object for accessing xattrs for a particular file.
     *
     * @param string $filename
     * @return File
     */
    public function file(string $filename): File
    {
        return new File($filename, $this);
    }

    /**
     * Determines if the attr command is available.
     *
     * @return bool
     */
    protected function hasAttr(): bool
    {
        return boolval(exec('which attr 2>/dev/null'));
    }
}
