<?php

declare(strict_types=1);

namespace Danjones\Xattr;

/**
 * Gives a more object-oriented file access to extended attributes.
 */
class File
{
    protected $filename;
    protected $xattr;

    /**
     * Constructor.
     */
    public function __construct(string $filename, Xattr $xattr = null)
    {
        if (!file_exists($filename)) {
            throw new \InvalidArgumentException('filename must be a valid filename');
        }

        $this->filename = $filename;
        $this->xattr = $xattr ?? new Xattr();
    }

    /**
     * Gets the filename.
     *
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * Gets the filename for the given destination.
     *
     * @param string|static $dest
     * @return string
     */
    protected function getDestination($dest): string
    {
        if ($dest instanceof self) {
            return $dest->getFilename();
        }

        if (!is_string($dest) || !file_exists($dest)) {
            throw new \InvalidArgumentException('Destination must be a filename, or an instance of ' . self::class);
        }

        return $dest;
    }

    /**
     * Copies all attributes to $dest
     *
     * @param string|static $dest
     * @return $this
     */
    public function clone($dest): self
    {
        $this->xattr->clone($this->filename, $this->getDestination($dest));

        return $this;
    }

    /**
     * Copies $key attributes to $dest
     *
     * @param string $key
     * @param string|static $dest
     * @return $this
     */
    public function copy(string $key, $dest): self
    {
        $this->xattr->copy($this->filename, $key, $this->getDestination($dest));

        return $this;
    }

    /**
     * Returns the $key attributes
     *
     * @param string $key
     * @return string|null
     */
    public function get(string $key): ?string
    {
        return $this->xattr->get($this->filename, $key);
    }

    /**
     * Sets the $key attributes to $value.
     *
     * If $value is null, or empty, remove the attribute.
     *
     * @param string $key
     * @param string $value
     * @return $this
     */
    public function set(string $key, ?string $value): self
    {
        $this->xattr->set($this->filename, $key, $value);

        return $this;
    }

    /**
     * Removes the $key attribute.
     *
     * @param string $key
     * @return $this
     */
    public function remove(string $key): self
    {
        $this->xattr->remove($this->filename, $key);

        return $this;
    }

    /**
     * Returns a list of all attribute names.
     *
     * @return array
     */
    public function list(): array
    {
        return $this->xattr->list($this->filename);
    }
}
